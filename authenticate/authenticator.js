/**
 * Authenticator
 * */

exports.ensureAuthenticated = function (req, res, next) {
    console.log(req.isAuthenticated())
    if (req.isAuthenticated()) {
        return next();
    } else {
        //req.flash('error_msg','You are not logged in');
        res.redirect('/kryptosattendance/dashboard/login');
    }
}