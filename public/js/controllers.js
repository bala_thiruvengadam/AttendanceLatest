'use strict';

/* Controllers */
var baseURI = "/kryptosattendance/attendance";
var attendanceModule = angular.module('myApp.controllers', []);
attendanceModule.controller('AppCtrl', function($scope, $routeParams, $compile, $http, $rootScope, $window, $location, $sce) {
    try {
        console.log("App ctrl called");
        console.log($routeParams.appid + " : " + $routeParams.pageid);
        $rootScope.appid = $routeParams.appid;
        $rootScope.pageid = $routeParams.pageid;
        var pageDefinition = function(appDefinition, pageId) {
            var pageDef;
            var i;
            for (i = 0; i < appDefinition.pages.length; i++) {
                var tempPage = appDefinition.pages[i]
                if (tempPage.pageid == pageId) {
                    //alert (tempApp.name);
                    pageDef = tempPage;
                    break;
                }
            }
            return pageDef;
        };
        $scope.init = function(app, appid1, pageid1) {
            console.log("Init called : " + appid1 + " : " + pageid1);
            console.log(app.title);
            if (app.appdef) {
                setTimeout(function() {

                    console.log(app.appdef.name);
                    var pagedef = app.appdef.pages[0];
                    if (appid1 == app.appdef.name) {
                        console.log("FOUND : " + appid1);
                        pagedef = pageDefinition(app.appdef, pageid1);
                    }
                    var appid = app.appdef.name;
                    $("#widget" + appid).html("");
                    console.log("page Def : " + pagedef);
                    var updatedprocessor = pagedef.pageprocessor.split("#appContent").join("#widget" + appid);
                    window.eval(updatedprocessor);
                    var pageProcessorName = 'pageprocessor' + pagedef.pageid;

                    if (window[pageProcessorName] != undefined) {
                        window[pageProcessorName](pagedef, $scope, $routeParams, $compile,
                            $http, $rootScope, $sce, $window, $location);
                        $scope.$apply();
                    }
                }, 100);
            }
        }
    } catch (ex) {
        console.log(ex);
    }

}).controller('HomeCtrl', function($rootScope, $scope, $http, $location, $window, $route, $compile) {
    try {

        console.log("home called");
        $scope.selectedYear = '';
        $scope.selectedBranch = '';
        $scope.selectedClass = '';
        $scope.downloadlink = '';
        $scope.qrcodeimage = '';
        $rootScope.user = {};


        $http.get("/kryptosattendance/attendance/userDetail").then(function(data) {
            console.log('user Detail data' + JSON.stringify(data));
            $rootScope.user = data.data;
            $scope.selectQR();

            $http.post("/kryptosattendance/attendance/getYear", {
                tenant: $rootScope.user.tenant
            }).success(function(data) {
                console.log(data);
                $rootScope.years = data.year;
            }).error(function(data) {
                console.log("Error " + data);
            });

        }, function() {
            console.log("Error ");
        });


        $scope.getTime = function(val) {
            return parseInt(val / 60) + ":" + val % 60;
        }


        $scope.generateqrcode = function() {
            if ($scope.qrID == '')
                return;
            console.log($scope.qrID);
            $http.post("/kryptosattendance/attendance/generateQrCode", {
                content: $scope.qrID
            }).success(function(data) {

                console.log(data);

                $scope.qrcodeimage = data.image;
            }).error(function(data) {
                console.log("Error " + data);
            });

        }

        $scope.selectQR = function() {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            //alert($scope.selectedYear);
            $http.post("/kryptosattendance/attendance/getQrCode", {
                tenant: $rootScope.user.tenant
            }).success(function(data) {
                console.log(data);
                $rootScope.qrCodes = data.QrCodes;
            }).error(function(data) {
                console.log("Error " + data);
            });

        }


        $scope.selectYear = function() {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            //alert($scope.selectedYear);
            console.log($rootScope.user.tenant);
            console.log($scope.selectedYear);
            if ($scope.selectedYear != '') {
                $http.post("/kryptosattendance/attendance/getBranch", {
                    tenant: $rootScope.user.tenant,
                    yearId: $scope.selectedYear

                }).success(function(data) {
                    console.log($rootScope.user.tenant);
                    console.log(data);
                    $rootScope.branches = data.Branches;
                }).error(function(data) {
                    console.log("Error " + data);
                });
            }
        }


        $scope.selectBranch = function() {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            //alert($scope.selectedBranch);
            if ($scope.selectedBranch != '') {
                $http.post("/kryptosattendance/attendance/getSubject", {
                    tenant: $rootScope.user.tenant,
                    branchId: $scope.selectedBranch
                }).success(function(data) {
                    console.log(data);
                    $rootScope.subjects = data.Subjects[0].subjects;
                }).error(function(data) {
                    console.log("Error " + data);
                });
            }
        }
        $scope.selectSubject = function() {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            if ($scope.selectedBranch != '' && $scope.selectedSubject != '') {
                $http.post("/kryptosattendance/attendance/getAllclassRecords", {
                    tenant: $rootScope.user.tenant,
                    branchId: $scope.selectedBranch,
                    subjectId: $scope.selectedSubject
                }).success(function(data) {
                    console.log(data);
                    $rootScope.classes = data.class;
                }).error(function(data) {
                    console.log("Error " + data);
                });
            }
        }

        $scope.selectClass = function() {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            if ($scope.selectedClass != '') {
                var sclass = $rootScope.classes[$scope.selectedClass];
                $scope.sclass = sclass;
                console.log(sclass);
                $scope.reporttitle = "Date/Time : " + sclass.date + " " + $scope.getTime(sclass.stTime) + " - " + $scope.getTime(sclass.endTime);
                $http.post("/kryptosattendance/attendance/getAttendanceRoster", sclass).success(function(data) {
                    console.log(data);

                    $rootScope.roster = data.roster;
                    $scope.totalCount = $rootScope.roster.length;
                    var pcount = 0;
                    var acount = 0;
                    $.each($rootScope.roster, function(i, val) {
                        if (val.status == 'present') {
                            pcount++;
                        } else {
                            acount++;
                        }
                    });
                    $scope.presentCount = pcount;
                    $scope.absentCount = acount;
                }).error(function(data) {
                    console.log("Error " + data);
                });

            }
        }

        $scope.downloadReport = function() {
            $scope.downloadlink = '';
            if ($scope.selectedClass != '') {
                var sclass = $rootScope.classes[$scope.selectedClass];
                $scope.sclass = sclass;
                console.log(sclass);
                $scope.reporttitle = "Date/Time : " + sclass.date + " " + $scope.getTime(sclass.stTime) + " - " + $scope.getTime(sclass.endTime);
                $http.post("/kryptosattendance/attendance/downloadReport", sclass).success(function(data) {
                    console.log(data);
                    $scope.downloadlink = data.url;
                    alert("Report generated successfully.");
                }).error(function(data) {
                    console.log("Error " + data);
                });
            }

        }
        $rootScope.trustHtml = function(html) {
            return $sce.trustAsHtml(html);
        }
        $BODY = $('body'),
            $MENU_TOGGLE = $('#menu_toggle'),
            $SIDEBAR_MENU = $('#sidebar-menu'),
            $SIDEBAR_FOOTER = $('.sidebar-footer'),
            $LEFT_COL = $('.left_col'),
            $RIGHT_COL = $('.right_col'),
            $NAV_MENU = $('.nav_menu'),
            $FOOTER = $('footer');
        var setContentHeight = function() {
            // reset height
            $RIGHT_COL.css('min-height', $(window).height());

            var bodyHeight = $BODY.outerHeight(),
                footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
                leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
                contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

            // normalize content
            contentHeight -= $NAV_MENU.height() + footerHeight;

            $RIGHT_COL.css('min-height', contentHeight);
        };

        $rootScope.sideClick = function($event) {
            console.log("clicked " + $event.target);
            var $li = $($event.target).parent();
            console.log($li);
            if ($li.is('.active')) {
                $li.removeClass('active active-sm');
                $('ul:first', $li).slideUp(function() {
                    setContentHeight();
                });
            } else {
                // prevent closing menu if we are on child menu
                if (!$li.parent().is('.child_menu')) {
                    $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                    $SIDEBAR_MENU.find('li ul').slideUp();
                }

                $li.addClass('active');

                $('ul:first', $li).slideDown(function() {
                    setContentHeight();
                });
            }

        }
    } catch (ex) {
        console.log(ex);
    }

}).controller('DashboardCtrl', function($rootScope, $scope, $routeParams, $http, $location, $window, $route, $compile) {
    // write Ctrl here
    console.log("DashboardCtrl called compile");
    var dbid = $routeParams.dbid;
    var dboard = null;
    console.log("dashboard");
    $http.get("/api/metadata/db").success(function(data) {
        console.log(data);
        $rootScope.dbdata = data;
    }).error(function(data) {
        console.log("Error " + data);
    });
}).controller('DetailsCtrl', function($rootScope, $scope, $routeParams, $http, $location, $window, $route, $compile) {
    try {

        console.log("DetailsCtrl called");
        $scope.selectedYear = '';
        $scope.selectedBranch = '';
        $scope.selectedClass = '';
        $scope.downloadlink = '';

        $http.post("/kryptosattendance/attendance/getYear", {
            tenant: $rootScope.user.tenant
        }).success(function(data) {
            console.log(data);
            $rootScope.years = data.year;
        }).error(function(data) {
            console.log("Error " + data);
        });


        $scope.getTime = function(val) {
            return parseInt(val / 60) + ":" + val % 60;
        }

        $scope.selectYear = function() {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            //alert($scope.selectedYear);
            if ($scope.selectedYear != '') {
                $http.post("/kryptosattendance/attendance/getBranch", {
                    tenant: $rootScope.user.tenant,
                    yearId: $scope.selectedYear
                }).success(function(data) {
                    console.log(data);
                    $rootScope.branches = data.Branches;
                }).error(function(data) {
                    console.log("Error " + data);
                });
            }
        }


        $scope.selectBranch = function() {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            //alert($scope.selectedBranch);
            if ($scope.selectedBranch != '') {
                $http.post("/kryptosattendance/attendance/getSubject", {
                    tenant: $rootScope.user.tenant,
                    branchId: $scope.selectedBranch
                }).success(function(data) {
                    console.log(data);
                    $rootScope.subjects = data.Subjects[0].subjects;
                }).error(function(data) {
                    console.log("Error " + data);
                });
            }
        }
        $scope.selectSubject = function() {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            if ($scope.selectedBranch != '' && $scope.selectedSubject != '') {
                $http.post("/kryptosattendance/attendance/getAllclassRecords", {
                    tenant: $rootScope.user.tenant,
                    branchId: $scope.selectedBranch,
                    subjectId: $scope.selectedSubject
                }).success(function(data) {
                    console.log(data);
                    $rootScope.classes = data.class;
                }).error(function(data) {
                    console.log("Error " + data);
                });
            }
        }

        $scope.selectClass = function() {
            $scope.downloadlink = '';
            $rootScope.roster = '';
            if ($scope.selectedClass != '') {
                var sclass = $rootScope.classes[$scope.selectedClass];
                $scope.sclass = sclass;
                console.log(sclass);
                $scope.reporttitle = "Date/Time : " + sclass.date + " " + $scope.getTime(sclass.stTime) + " - " + $scope.getTime(sclass.endTime);
                $http.post("/kryptosattendance/attendance/getAttendanceRoster", sclass).success(function(data) {
                    console.log(data);

                    $rootScope.roster = data.roster;
                    $scope.totalCount = $rootScope.roster.length;
                    var pcount = 0;
                    var acount = 0;
                    $.each($rootScope.roster, function(i, val) {
                        if (val.status == 'present') {
                            pcount++;
                        } else {
                            acount++;
                        }
                    });
                    $scope.presentCount = pcount;
                    $scope.absentCount = acount;
                }).error(function(data) {
                    console.log("Error " + data);
                });

            }
        }

        $scope.downloadReport = function() {
            $scope.downloadlink = '';
            if ($scope.selectedClass != '') {
                var sclass = $rootScope.classes[$scope.selectedClass];
                $scope.sclass = sclass;
                console.log(sclass);
                $scope.reporttitle = "Date/Time : " + sclass.date + " " + $scope.getTime(sclass.stTime) + " - " + $scope.getTime(sclass.endTime);
                $http.post("/kryptosattendance/attendance/downloadReport", sclass).success(function(data) {

                    console.log(JSON.stringify(data));

                    var anchor = angular.element('<a/>');
                    anchor.attr({
                        href: '/kryptosattendance/attendance/downloadReport/' + sclass.tenant,
                        target: '_blank'
                    })[0].click();


                    alert("Report generated successfully.");
                }).error(function(data) {
                    console.log("Error " + data);
                });
            }

        }


        $rootScope.trustHtml = function(html) {
            return $sce.trustAsHtml(html);
        }
        $BODY = $('body'),
            $MENU_TOGGLE = $('#menu_toggle'),
            $SIDEBAR_MENU = $('#sidebar-menu'),
            $SIDEBAR_FOOTER = $('.sidebar-footer'),
            $LEFT_COL = $('.left_col'),
            $RIGHT_COL = $('.right_col'),
            $NAV_MENU = $('.nav_menu'),
            $FOOTER = $('footer');
        var setContentHeight = function() {
            // reset height
            $RIGHT_COL.css('min-height', $(window).height());

            var bodyHeight = $BODY.outerHeight(),
                footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
                leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
                contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

            // normalize content
            contentHeight -= $NAV_MENU.height() + footerHeight;

            $RIGHT_COL.css('min-height', contentHeight);
        };

        $rootScope.sideClick = function($event) {
            console.log("clicked " + $event.target);
            var $li = $($event.target).parent();
            console.log($li);
            if ($li.is('.active')) {
                $li.removeClass('active active-sm');
                $('ul:first', $li).slideUp(function() {
                    setContentHeight();
                });
            } else {
                // prevent closing menu if we are on child menu
                if (!$li.parent().is('.child_menu')) {
                    $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                    $SIDEBAR_MENU.find('li ul').slideUp();
                }

                $li.addClass('active');

                $('ul:first', $li).slideDown(function() {
                    setContentHeight();
                });
            }

        }
    } catch (ex) {
        console.log(ex);
    }

    console.log("DetailsCtrl called compile");
}).controller('CancelclassesCtrl', function($rootScope, $scope, $routeParams, $http, $location, $window, $route, $compile) {
    // write Ctrl here
    $scope.custFormat = function(time) {
        return (Math.floor(time / 60)) + ":" + (time % 60 == 0 ? "00" : time % 60);
    };
    try {

        console.log("CancelclassesCtrl called");
        $http.post("/kryptosattendance/attendance/getcancelClassesRecord", {
            tenant: $rootScope.user.tenant
        }).success(function(data) {
            console.log(data)

            $rootScope.cancelclasses = data.cancelclasses;
        }).error(function(data) {
            console.log("Error " + data);
        });

    } catch (ex) {
        console.log(ex);
    }


}).controller('AdminCtrl', function($rootScope, $scope, $routeParams, $http, $location, $window, $route, $compile) {
    // write Ctrl here
    console.log("AdminCtrl called compile");
    $scope.selectApp = function() {
        alert("called  " + $scope.seletedapp);
        $http.get("/api/kryptos/livedata/" + $scope.seletedapp).success(function(data) {
            console.log(data);
            $scope.selectedappdata = data;
        }).error(function(data) {
            console.log("Error " + data);
        });

    }

    $http.get("/api/kryptos/listMyApps").success(function(data) {
        console.log(data);
        $scope.myapps = data;
    }).error(function(data) {
        console.log("Error " + data);
    });
});

attendanceModule.controller("MonthlyReportsCtrl", ["$rootScope", "$scope", "$routeParams", "$http", "$location", "$window", "$route", "$compile", function($rootScope, $scope, $routeParams, $http, $location, $window, $route, $compile) {

    $("#rdbtn").addClass("hide");
    var stDatePicker = $("#startDate").datepicker({
        format: "yyyy/mm",
        viewMode: "months",
        minViewMode: "months"
    });
    var endDatePicker = $("#endDate").datepicker({
        format: "yyyy/mm",
        viewMode: "months",
        minViewMode: "months"
    });

    $http.get("/kryptosattendance/attendance/userDetail").then(function(data) {
        console.log('user Detail data' + JSON.stringify(data));
        $rootScope.user = data.data;
        //$scope.selectQR();

        $http.post("/kryptosattendance/attendance/getYear", {
            tenant: $rootScope.user.tenant
        }).success(function(data) {
            console.log(data);
            $rootScope.years = data.year;
        }).error(function(data) {
            console.log("Error " + data);
        });

    }, function() {
        console.log("Error ");
    });


    $scope.selectYear = function() {
        $scope.downloadlink = '';
        $rootScope.roster = '';
        //alert($scope.selectedYear);
        if ($scope.selectedYear != '') {
            $http.post("/kryptosattendance/attendance/getBranch", {
                tenant: $rootScope.user.tenant,
                yearId: $scope.selectedYear
            }).success(function(data) {
                console.log(data);
                $rootScope.branches = data.Branches;
            }).error(function(data) {
                console.log("Error " + data);
            });
        }
    }


    $scope.selectBranch = function() {
        $scope.downloadlink = '';
        $rootScope.roster = '';
        //alert($scope.selectedBranch);
        if ($scope.selectedBranch != '') {
            $http.post("/kryptosattendance/attendance/getSubject", {
                tenant: $rootScope.user.tenant,
                branchId: $scope.selectedBranch
            }).success(function(data) {
                console.log(data);
                $rootScope.subjects = data.Subjects[0].subjects;
            }).error(function(data) {
                console.log("Error " + data);
            });
        }
    }
    $scope.selectSubject = function() {
        $scope.downloadlink = '';
        $rootScope.roster = '';
        if ($scope.selectedBranch != '' && $scope.selectedSubject != '') {
            $http.post("/kryptosattendance/attendance/getAllclassRecords", {
                tenant: $rootScope.user.tenant,
                branchId: $scope.selectedBranch,
                subjectId: $scope.selectedSubject
            }).success(function(data) {
                console.log(data);
                $rootScope.classes = data.class;
            }).error(function(data) {
                console.log("Error " + data);
            });
        }
    };


    $scope.getDate=function(dateStr){
        dateStr = dateStr+"/1";
        return new Date(dateStr);
    };

    $scope.downloadMonthlyReport = function() {

    var table = TableExport(document.getElementById("tbdownload"), {
        headers: true, // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
        footers: true, // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
        formats: ['xls'], // (String[]), filetype(s) for the export, (default: ['xls', 'csv', 'txt'])
        filename: 'stats', // (id, String), filename for the downloaded file, (default: 'id')
        bootstrap: true, // (Boolean), style buttons using bootstrap, (default: true)
        exportButtons: true, // (Boolean), automatically generate the built-in export buttons for each of the specified formats (default: true)
        position: 'bottom', // (top, bottom), position of the caption element relative to table, (default: 'bottom')
        ignoreRows: null, // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
        ignoreCols: null, // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
        trimWhitespace: true // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: false)
    });
        /* var data = {
             tenant: $rootScope.user.tenant,
             branchId: $scope.selectedBranch,
             subjectId: $scope.selectedSubject,
             year: $scope.selectedYear,
             startDate: $scope.startDate,
             endDate: $scope.endDate
         };

         if (!(data.tenant && data.branchId && data.subjectId && data.year && data.startDate && data.endDate)) {
             console.log("bad request", data);
             return;
         }
          var startDate=$scope.startDate+"/01";
         var endDate = new Date($scope.endDate);
         endDate= new Date(endDate.getFullYear(),endDate.getMonth()+1,0);
         var endDate = endDate.getFullYear()+"/"+(endDate.getMonth()+1)+"/"+endDate.getDate();
         data.startDate = startDate;
         data.endDate = endDate;
         $http.post(baseURI + "/downloadStatsReports", data).then(function(res) {
             if (res.data.status == "error") {
                 alert("Some error occured in report generation");
             }
         }, function(err) {
             alert("Some error occured in report generation");
         });*/
        /*var data = {
            tenant: $rootScope.user.tenant,
            branchId: $scope.selectedBranch,
            subjectId: $scope.selectedSubject,
            year: $scope.selectedYear,
            startDate: $scope.startDate,
            endDate: $scope.endDate
        };

        if (!(data.tenant && data.branchId && data.subjectId && data.year && data.startDate && data.endDate)) {
            console.log("bad request", data);
            return;
        }

        //$("#tbdownload").addClass("hide");

        var startDate=$scope.startDate+"/01";
        var endDate = new Date($scope.endDate);
        endDate= new Date(endDate.getFullYear(),endDate.getMonth()+1,0);
        var endDate = endDate.getFullYear()+"/"+(endDate.getMonth()+1)+"/"+endDate.getDate();
        data.startDate = startDate;
        data.endDate = endDate;
        $http.post(baseURI + "/studentMonthlyStats", data).then(function(res) {
            var data = res.data;
            if (data.status == "success") {
                $scope.roster = true;
                $scope.studentStats = data.data;
                  //var table = TableExport(document.getElementById("tbdownload"));   
                 //var exportData = table.getExportData(); 
                //table.remove();
                $("table").tableExport();
            } else {
                console.log("Data : ", data);
                alert(data.message);
            }
        }, function(err) {
            console.log("err", err);
        });*/
        //var exportData = table.getExportData();
        
    };
    $scope.fetchMonthlyData = function() {
        $scope.studentStats = [];
        $scope.studentStats
        var data = {
            tenant: $rootScope.user.tenant,
            branchId: $scope.selectedBranch,
            subjectId: $scope.selectedSubject,
            year: $scope.selectedYear,
            startDate: $scope.startDate,
            endDate: $scope.endDate
        };

        if (!(data.tenant && data.branchId && data.subjectId && data.year && data.startDate && data.endDate)) {
            console.log("bad request", data);
            return;
        }

        var startDate = $scope.startDate + "/01";
        var endDate = new Date($scope.endDate);
        endDate = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 0);
        var endDate = endDate.getFullYear() + "/" + (endDate.getMonth() + 1) + "/" + endDate.getDate();
        data.startDate = startDate;
        data.endDate = endDate;
        $http.post(baseURI + "/studentMonthlyStats", data).then(function(res) {
            var data = res.data;
            if (data.status == "success") {
                $("#rdbtn").removeClass("hide");
                $scope.roster = true;
                $scope.studentStats = data.data;
            } else {
                console.log("Data : ", data);
                alert(data.message);
            }
        }, function(err) {
            console.log("err", err);
        });
    };

    $scope.sortMonthsData = function(months) {
        months.sort((a, b) => {
            return (a < b ? -1 : (a > b ? 1 : 0));
        });
        return months;
    };

}]);