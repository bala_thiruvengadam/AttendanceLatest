var bcrypt = require('bcrypt');
var $q = require('q');
const saltRounds = 10;

module.exports.encryptPassword = function(password) {
	var $passPromose = $q.defer();
	//console.log("pass promise");
	bcrypt.genSalt(saltRounds, function(err, salt) {
		bcrypt.hash(password, salt, function(err, hash) {
			// Store hash in your password DB.
			if (err) {
				$passPromose.reject(err);
			} else {
				console.log("resoling promise");
				$passPromose.resolve({
					hash: hash,
					salt: salt
				});
			}
		});
	});
	return $passPromose.promise;

}

module.exports.comparePasswords = function(password, hash) {
	var $passPromose = $q.defer();
	bcrypt.compare(password, hash, function(err, res) {
		if (err || res == false) {
			$passPromose.reject(false);
		} else {
			$passPromose.resolve(true);
		}
	});
	return $passPromose.promise;
};

