var EVCDateUtils =function(){
	var dateUtils = this;
	dateUtils.convertddMMyyyyToyyyyMMdd=function(date){
		if(date.indexOf("/")>2){
			return date;
		}
		var datePart = date.split("/");
		return datePart[2]+"/"+ datePart[1]+"/"+datePart[0];
		
	};

	dateUtils.convertddMMyyyyToyyyyMMddFromObj=function(date){
		
		return date.getFullYear()+"/"+ (date.getMonth()+1)+"/"+date.getDate();
		
	};
} 

module.exports=new EVCDateUtils();