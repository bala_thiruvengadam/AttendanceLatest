var $q = require('q');
var fs = require("fs");
var jsonfile = require('jsonfile');
var excelbuilder = require('msexcel-builder');
var dbUtil = require("../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var MongoClient = require('mongodb').MongoClient;
var moment = require('moment-timezone');
var async = require('async');
var evDateUtil = require('../utils/EVCDateUtils');
var bcryptUtils = require('../utils/bcryptutils');

exports.migratePasswords = function(req, res) {

	var mgPassPromise = $q.defer();
	dbUtil.getConnection(function(db) {
		var userCollection = db.collection("T_UserRecords");
		userCollection.find({}).toArray(function(err, docs) {
			var updateCnt = 0,
				errCnt = 0;
			docs.forEach(function(doc) {
				console.log("result ", doc.password);
				bcryptUtils.encryptPassword(doc.password).then(function(data) {
					dbUtil.getConnection(function(db2) {
						var userCollection2 = db2.collection("T_UserRecords");

						userCollection2.update({
								_id: doc._id
							}, {
								$set: {
									hash: data.hash,
									salt: data.salt
								},
								$unset: {
									password: 1
								}
							},
							function(err, updResult) {
								console.log("Error");
								if (err) {
									errCnt++;
								} else {
									updateCnt++;
								}

								if ((updateCnt + errCnt) == docs.length) {
									if (updateCnt == docs.length) {
										mgPassPromise.resolve("Docs updated " + updateCnt);
									} else {
										mgPassPromise.reject("Docs updated " + updateCnt + "  Docs failed " + errCnt);
									}
								}
							}
						);
					});
				}, function(err) {
					console.log("encrypt err");
					errCnt++;
					mgPassPromise.reject(err);
				});
			});

		});
	});



	return mgPassPromise.promise;


}


/*exports.migrateDates = function() {
    var migrateCount = 0;
    console.log("Starting migrate");

    try {
        dbUtil.getConnection(function(db) {

            db.listCollections().toArray(function(err, collections) {

                collections.forEach(function(item) {
                    var collName = item.name;
                    console.log("collection ", collName);
                    if (item.name.endsWith("AttendanceRecords")) {
                        db.collection(collName).find().toArray(function(err, docs) {
                            docs.forEach(function(doc) {
                                var date = evDateUtil.convertddMMyyyyToyyyyMMdd(doc.date);
                                dbUtil.getConnection(function(db) {
                                    db.collection(collName).update({
                                        _id: doc._id
                                    }, {
                                        $set: {
                                            date: date
                                        }
                                    }, function(err, result) {
                                        if (!err) {
                                            migrateCount++;
                                        } else {
                                            console.log("Error : ", err);
                                        }
                                    });
                                });
                            });
                        });
                    }
                });
            });
        });
    } catch (e) {
        res.send("error", e);
    }

    console.log("Total docs migrated  :: ", migrateCount);
    res.send("Total docs migrated  :: ", migrateCount);

}
*/