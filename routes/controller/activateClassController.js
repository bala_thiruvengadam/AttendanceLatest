"use strict";
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var moment = require('moment-timezone');
var evDateUtil = require('../../utils/EVCDateUtils');


exports.activateClass = function(req, res, next) {
    if (req.body && req.body.qrCode && req.body.stTime && req.body.endTime && req.body.branchName && req.body.branchId && req.body.subject && req.body.subjectId && req.body.year && req.body.facultyName && req.body.tenant) {

        var qrCode = req.body.qrCode
        var branchName = req.body.branchName
        var branchId = req.body.branchId
        var year = req.body.year
        var subject = req.body.subject
        var subjectId = req.body.subjectId
        var facultyName = req.body.facultyName
        var tenant = req.body.tenant
        var endTime = req.body.endTime
        var stTime = req.body.stTime


        var date = moment().utcOffset("+05:30").format();
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hh = date.split('T')[1].split(':')[0];
        var minutes = date.split('T')[1].split(':')[1];
        var sec = date.split('T')[1].split(':')[2].substr(0, 2);
        //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
        var today = new Date(yyyy, mm - 1, dd, hh, minutes, sec);



        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0, so always add + 1

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var date = dd + '/' + mm + '/' + yyyy;

        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();



        var stuTime = today.getHours() * 60 + today.getMinutes();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var date = dd + '/' + mm + '/' + yyyy;
        var currentTmInMin = today.getHours() * 60 + today.getMinutes();

        /*if(currentTmInMin >= endTime || currentTmInMin >= endTime-5){
            res.json({
                "error": "Class End time has already expired."
            });
        }*/
        date = evDateUtil.convertddMMyyyyToyyyyMMdd(date);
        if (false) {
            res.json({
                "error": "Class End time has already expired."
            });
        } else {
            try {
                var dateTimeStamp = new Date();
                var data = {
                    'qrCode': qrCode,
                    'branchName': branchName,
                    'branchId': branchId,
                    'year': year,
                    'subject': subject,
                    'subjectId': subjectId,
                    'facultyName': facultyName,
                    'stTime': stTime,
                    'endTime': endTime,
                    'date': date,
                    'activatedtime': dateTimeStamp.toString()
                };

                var qrCodeQuery = {
                    $and: [{
                            "qrCode": qrCode
                        }, {
                            "year": year
                        }, {
                            "subjectId": subjectId
                        }, {
                            "branchId": branchId
                        }, {
                            "date": date
                        },

                        {
                            $or: [{
                                "stTime": {
                                    $gte: stTime,
                                    $lte: endTime
                                }
                            }, {
                                "endTime": {
                                    $gte: stTime,
                                    $lte: endTime
                                }
                            }, {
                                "stTime": {
                                    $lte: stTime
                                },
                                "endTime": {
                                    $gte: endTime
                                }
                            }, {
                                "stTime": {
                                    $gte: stTime
                                },
                                "endTime": {
                                    $lte: endTime
                                }
                            }]
                        }
                    ]
                };
                dbUtil.getConnection(function(db) {
                    var tableName = "T_" + tenant + "_activeQrCodes";
                    db.collection(tableName).find(qrCodeQuery).toArray(function(err, result) {
                        if (result.length > 0) {
                            res.json({
                                "activeClass": result
                            });
                        } else {
                            db.collection(tableName).insertOne(data, function(err, result3) {
                                res.json({
                                    "success": "class activated"
                                });
                            });
                        }
                    });
                });
            } catch (e) {
                res.json({
                    "error": "Some error occurred. Please try again."
                });
            }
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}

exports.getActiveQRCodes = function(req, res, next) {
    if (req.body && req.body.tenant) {

        var tenant = req.body.tenant
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_activeQrCodes";
                db.collection(tableName).find({}).toArray(function(err, result) {
                    if (result.length > 0) {
                        res.json({
                            "success": result
                        });
                    } else {
                        res.json({
                            "error": "No active QR"
                        });
                    }
                });
            });

        } catch (e) {
            res.json({
                "error": "some error occurred."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}



exports.getClassDetail = function(req, res, next) {
    if (req.body && req.body.qrCode && req.body.tenant) {

        var qrCode = req.body.qrCode
        var tenant = req.body.tenant
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_activeQrCodes";
                db.collection(tableName).find({
                    "qrCode": qrCode
                }).toArray(function(err, result) {
                    if (result.length > 0) {
                        res.json({
                            "success": result
                        });
                    } else {
                        res.json({
                            "error": "Qr Code is not activated."
                        });
                    }
                });
            });

        } catch (e) {
            res.json({
                "error": "Unable to read timetable."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}

exports.deactivateClass = function(req, res, next) {
    if (req.body && req.body.tenant) {
        var tenant = req.body.tenant;

        var date = moment().utcOffset("+05:30").format();
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hh = date.split('T')[1].split(':')[0];
        var minutes = date.split('T')[1].split(':')[1];
        var sec = date.split('T')[1].split(':')[2].substr(0, 2);
        //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
        var today = new Date(yyyy, mm - 1, dd, hh, minutes, sec);



        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0, so always add + 1

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var date = dd + '/' + mm + '/' + yyyy;

        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();



        var currentTmInMin = today.getHours() * 60 + today.getMinutes();

        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_activeQrCodes";
                db.collection(tableName).remove({
                    'endTime': {
                        $lte: currentTmInMin
                    }
                }, function(err, result) {
                    res.json({
                        "success": result
                    });
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}

exports.deactivateClassManually = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.qrCode) {
        var tenant = req.body.tenant;
        var qrCode = req.body.qrCode;
        var d = new Date();
        var currentTmInMin = d.getHours() * 60 + d.getMinutes();
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_activeQrCodes";
                db.collection(tableName).remove({
                    'qrCode': qrCode
                }, function(err, result) {
                    res.json({
                        "success": result
                    });
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.classRecords = function(req, res, next) {
    if (req.body) {
        console.log(req.body);
        var data = req.body;
        data.status = "notSubmitted";

        var date = moment().utcOffset("+05:30").format();
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hh = date.split('T')[1].split(':')[0];
        var minutes = date.split('T')[1].split(':')[1];
        var sec = date.split('T')[1].split(':')[2].substr(0, 2);
        //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
        var today = new Date(yyyy, mm - 1, dd, hh, minutes, sec);



        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0, so always add + 1

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var date = dd + '/' + mm + '/' + yyyy;

        date = evDateUtil.convertddMMyyyyToyyyyMMdd(date);
        data.date = date;
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + req.body.tenant + "_classRecords";
                db.collection(tableName).insertOne(data, function(err, result3) {
                    res.json({
                        "success": "class record added to unsubmitted"
                    });
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}

exports.getclassRecords = function(req, res, next) {
    if (req.body) {
        console.log(req.body);

        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + req.body.tenant + "_classRecords";
                db.collection(tableName).find({
                    "facultyEmail": req.body.facultyEmail,
                    "status": "notSubmitted"
                }).toArray(function(err, result) {
                    console.log(result);
                    if (result.length == 0) {
                        res.json({
                            "class": []
                        });
                    } else {
                        res.json({
                            "class": result
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.updateClassRecords = function(req, res, next) {
    if (req.body) {
        console.log(req.body);

        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + req.body.tenant + "_classRecords";

                db.collection(tableName).updateOne({
                    "facultyEmail": req.body.facultyEmail,
                    "status": "notSubmitted"
                }, {
                    $set: {
                        'status': 'submitted'
                    }
                }, function(err, result) {
                    res.json({
                        "success": "class record updated"
                    })
                });


            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}

exports.checkSubmitClassRecord = function(req, res, next) {
    if (req.body) {
        req.body.status = "submitted";
        console.log(req.body);
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + req.body.tenant + "_classRecords";
                db.collection(tableName).find(req.body).toArray(function(err, result) {
                    console.log(result);
                    if (result.length == 0) {
                        res.json({
                            "class": []
                        });
                    } else {
                        res.json({
                            "class": result
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}



exports.checkClassActivated = function(req, res, next) {
    if (req.body) {
        req.body.status = "notSubmitted";
        console.log(req.body);
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + req.body.tenant + "_classRecords";
                db.collection(tableName).find(req.body).toArray(function(err, result) {
                    console.log(result);
                    if (result.length == 0) {
                        res.json({
                            "class": []
                        });
                    } else {
                        res.json({
                            "class": result
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.getAllclassRecords = function(req, res, next) {
    if (req.body) {
        console.log(req.body);

        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + req.body.tenant + "_classRecords";
                db.collection(tableName).find(req.body).toArray(function(err, result) {
                    console.log(result);
                    if (result.length == 0) {
                        res.json({
                            "class": []
                        });
                    } else {
                        res.json({
                            "class": result
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}



exports.AddDetails = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.email && req.body.password) {

        var tenant = req.body.tenant
        var email = req.body.email
        var password = req.body.password
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + "UserRecords";
                db.collection(tableName).find({
                    email: email
                }).toArray(function(err, result) {
                    console.log(result);
                    if (result.length == 0) {

                        db.collection(tableName).insertOne({
                            "tenant": tenant,
                            "email": email,
                            "password": password
                        }, function(err, result3) {
                            res.json({
                                status: true,
                                message: 'User registered successfully'
                            });
                        });
                    } else {
                        res.json({
                            status: false,
                            message: 'User already registered'
                        });

                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    }

}