"use strict";
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;

exports.addSubject = function(req, res, next) {
    if (req.body && req.body.branchName && req.body.branchId && req.body.subjectName && req.body.tenant) {

        var branchName = req.body.branchName
        var branchId = req.body.branchId
        var subjectName = req.body.subjectName
        var tenant = req.body.tenant
        try {
            var data = {
                'subjectName': subjectName,
                'subjectId': 'subject'+Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1)
            };
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_Branches";
                db.collection(tableName).find({
                    "branchId": branchId
                }).toArray(function(err, result) {
                    if (result.length > 0) {
                        var tableName = "T_" + tenant + "_Subjects";
                        db.collection(tableName).find({
                            "branchId": branchId
                        }).toArray(function(err, result) {
                            if (result.length > 0) {
                                var branchSubjects = result[0];
                                branchSubjects.subjects.push(data);

                                db.collection(tableName).updateOne({
                                    "branchId": branchId
                                }, {
                                    $set: branchSubjects
                                }, function(err, result3) {
                                    res.json({
                                        "success": "subject added successfully"
                                    });
                                });
                            } else {
                                var subjectData = [];
                                subjectData.push(data);
                                var branchSubjects={
                                    "branchId":branchId,
                                    "branchName":branchName,
                                    "subjects":subjectData
                                }
                                db.collection(tableName).insertOne(branchSubjects, function(err, result3) {
                                    res.json({
                                        "success": "subject added successfully"
                                    });
                                });
                            }
                        });
                    } else {


                        res.json({
                            "error": "Please add branch/stream before adding subjects"
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}

exports.findSubjectFaculty=function(req,res){
    if(!(req.body.tenant && req.body.year && req.body.subjectId && req.body.branchId)){
        res.status(403).send("Bad Request");
    }
    var tableName = "T_" + req.body.tenant +"_classRecords";
      dbUtil.getConnection(function(db) {
        var collection = db.collection(tableName);
        var response = {};
        collection.findOne({subjectId:req.body.subjectId,year:req.body.year,branchId:req.body.branchId},function(err,result){
            if(err){
                response.status ="error";
                response.message ="Something went wrong";
                res.json(response);
            }else{
                response.status = "success";
                response.data = result;
                res.json(response);
            }
        })
      });
}


exports.getSubject = function(req, res, next) {
    console.log("Subject Req:: ",req.body);
    if (req.body.tenant && req.body.branchId) {
        var tenant = req.body.tenant
        var branchId = req.body.branchId
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_Subjects";
                db.collection(tableName).find({"branchId":branchId}).toArray(function(err, result) {
                    if (result.length > 0) {
                        res.json({
                            'Subjects': result
                        });
                    } else {
                        res.json({
                            "error": "No branch added"
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}