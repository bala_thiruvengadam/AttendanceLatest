"use strict";
//var GoogleSpreadsheet = require("google-spreadsheet");
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var moment = require('moment-timezone');

exports.getClassStudents = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.yearId && req.body.branch && req.body.subject) {

        var tenant = req.body.tenant
        var yearId = req.body.yearId
        var branch = req.body.branch
        var subject = req.body.subject

        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_" + yearId + "_" + branch + "_" + subject ;
                db.collection(tableName).find({}).toArray(function(err, result) {
                    if (result.length > 0) {
                        res.json({
                            "studentsList": result
                        });
                    } else {
                        res.json({
                            "error": "No Students in the list"
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}

exports.timezoneapi = function(req, res, next) {
    var today = new Date();
    var date = moment().utcOffset("+05:30").format();
    /* date will a string like-  2016-07-08T18:09:18+05:30   */
    
    console.log(date);
    /* get year,month,day,hours,minutes from date string */
    var yyyy = date.substring(0, date.indexOf("-"));
    var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
    var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
    var hh = date.split('T')[1].split(':')[0];
    var minutes = date.split('T')[1].split(':')[1];
    var sec = date.split('T')[1].split(':')[2].substr(0, 2);
    //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
    var t = new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
    console.log(t+" "+date);

}