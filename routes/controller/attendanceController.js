"use strict";
var q = require('q');
var fs = require("fs");
var jsonfile = require('jsonfile');
var excelbuilder = require('msexcel-builder');
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var MongoClient = require('mongodb').MongoClient;
var moment = require('moment-timezone');
var async = require('async');
var evDateUtil = require('../../utils/EVCDateUtils');
var attendanceSvc = require('../../services/attendanceSvc');
var migrationSvc = require('../../services/migrationSvc');
var streams = require('memory-streams');


var path = require("path");
var uuid = require("node-uuid");

var weekday = new Array(7);
weekday[0] = "Monday";
weekday[1] = "Tuesday";
weekday[2] = "Wednesday";
weekday[3] = "Thursday";
weekday[4] = "Friday";
weekday[5] = "Saturday";
weekday[6] = "Sunday";


exports.migrateDates = function(req, res) {
    var migrateCount = 0;
    console.log("Starting migrate");

    try {
        dbUtil.getConnection(function(db) {

            db.listCollections().toArray(function(err, collections) {

                collections.forEach(function(item) {
                    var collName = item.name;
                    console.log("collection ", collName);

                    if (item.name.endsWith("AttendanceRecords") || item.name.endsWith("_activeQrCodes") || item.name.endsWith("_classRecords") || item.name.endsWith("_CancelledClasses") || item.name.endsWith("UserCancelClass")) {
                        db.collection(collName).find().toArray(function(err, docs) {
                            docs.forEach(function(doc) {
                                var date = evDateUtil.convertddMMyyyyToyyyyMMdd(doc.date);
                                dbUtil.getConnection(function(db) {
                                    db.collection(collName).update({
                                        _id: doc._id
                                    }, {
                                        $set: {
                                            date: date
                                        }
                                    }, function(err, result) {
                                        if (!err) {
                                            migrateCount++;
                                        } else {
                                            console.log("Error : ", err);
                                        }
                                    });
                                });
                            });
                        });
                    }
                });
            });
        });
    } catch (e) {
        res.send("error", e);
    }

    console.log("Total docs migrated  :: ", migrateCount);
    res.send("Total docs migrated  :: ", migrateCount);

}

exports.markUserAttendance = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.qrCode && req.body.faculty && req.body.classStTime && req.body.classEndTime && req.body.stuName && req.body.branchName && req.body.branchId && req.body.subject && req.body.subjectId && req.body.year && req.body.rollNo && req.body.email) {

        var tenant = req.body.tenant;
        var qrCode = req.body.qrCode;
        var faculty = req.body.faculty;
        var classStTime = req.body.classStTime;
        var classEndTime = req.body.classEndTime;
        var stuName = req.body.stuName;
        var branchName = req.body.branchName;
        var branchId = req.body.branchId;
        var subject = req.body.subject;
        var subjectId = req.body.subjectId;
        var year = req.body.year;
        var rollNo = req.body.rollNo;
        var email = req.body.email;
        var role = req.body.role;


        var status = "present";

        var date = moment().utcOffset("+05:30").format();
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hh = date.split('T')[1].split(':')[0];
        var minutes = date.split('T')[1].split(':')[1];
        var sec = date.split('T')[1].split(':')[2].substr(0, 2);
        //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
        var today = new Date(yyyy, mm - 1, dd, hh, minutes, sec);


        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0, so always add + 1

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var date = dd + '/' + mm + '/' + yyyy;

        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();


        var stuTime = today.getHours() * 60 + today.getMinutes();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var date = dd + '/' + mm + '/' + yyyy;
        date = evDateUtil.convertddMMyyyyToyyyyMMdd(date);

        try {
            console.log(classStTime + " " + stuTime);

            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "AttendanceRecords";

                var query = {
                    "date": date,
                    "email": email,
                    "rollNo": rollNo
                };

                db.collection(tableName).find(query).toArray(function(err, result) {
                    console.log(result);
                    if (result.length > 0) {

                        if (classStTime > stuTime) {
                            res.json({
                                "error": "Class time not started yet."
                            });
                        }
                        var StudentMarkedAttendance = false;
                        //console.log(classTm);
                        /*var classTm = classTime.replace(/ /, "").split("-");
                         var classSTTime = classTm[0];
                         var classETTime = classTm[1];
                         var classSTTimeInMin = parseInt(classSTTime.split(":")[0]) * 60 + parseInt(classSTTime.split(":")[1])
                         var classETTimeInMin = parseInt(classETTime.split(":")[0]) * 60 + parseInt(classETTime.split(":")[1])*/

                        for (var i = 0; i < result.length; i++) {
                            var stuClassTime = result[i].stuTime;

                            //var StuTimeInMin = parseInt(stuClassTime.split(":")[0]) * 60 + parseInt(stuClassTime.split(":")[1])
                            if (stuClassTime <= classStTime ) {
                                res.json({
                                    "error": "You have already marked the attendance"
                                });
                                break;
                            } else {
                                StudentMarkedAttendance = true;
                            }
                        }
                        if (StudentMarkedAttendance) {
                            var data = {
                                'qrCode': qrCode,
                                'faculty': faculty,
                                'classStTime': classStTime,
                                'classEndTime': classEndTime,
                                'stuName': stuName,
                                'branchName': branchName,
                                'subject': subject,
                                'year': year,
                                'rollNo': rollNo,
                                'email': email,
                                'stuTime': stuTime,
                                'date': date,
                                'status': status

                            };
                            db.collection(tableName).insertOne(data, function(err, result3) {
                                if (!err) {
                                    res.json({
                                        "success": "Attendance marked"
                                    });
                                }
                            });
                        }
                    } else {
                        console.log('else');
                        if (stuTime >= classEndTime) {
                            res.json({
                                "error": "You have exceded time limit to mark attendance"
                            });
                        } else {

                            var data = {
                                'qrCode': qrCode,
                                'faculty': faculty,
                                'classStTime': classStTime,
                                'classEndTime': classEndTime,
                                'stuName': stuName,
                                'branchName': branchName,
                                'subject': subject,
                                'year': year,
                                'rollNo': rollNo,
                                'email': email,
                                'stuTime': stuTime,
                                'date': date,
                                'status': status
                            };
                            db.collection(tableName).insertOne(data, function(err, result3) {
                                if (!err) {
                                    res.json({
                                        "success": "Attendance marked"
                                    });
                                }
                            });
                        }
                    }
                });
            });


        } catch (e) {
            console.log(e)
        }
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}

exports.unMarkUserAttendance = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.qrCode && req.body.faculty && req.body.classStTime && req.body.classEndTime && req.body.stuName && req.body.branchName && req.body.branchId && req.body.subject && req.body.subjectId && req.body.year && req.body.rollNo && req.body.email) {

        var tenant = req.body.tenant;
        var qrCode = req.body.qrCode;
        var faculty = req.body.faculty;
        var classStTime = req.body.classStTime;
        var classEndTime = req.body.classEndTime;
        var stuName = req.body.stuName;
        var branchName = req.body.branchName;
        var branchId = req.body.branchId;
        var subject = req.body.subject;
        var subjectId = req.body.subjectId;
        var year = req.body.year;
        var rollNo = req.body.rollNo;
        var email = req.body.email;


        var status = "absent";

        var date = moment().utcOffset("+05:30").format();
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hh = date.split('T')[1].split(':')[0];
        var minutes = date.split('T')[1].split(':')[1];
        var sec = date.split('T')[1].split(':')[2].substr(0, 2);
        //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
        var today = new Date(yyyy, mm - 1, dd, hh, minutes, sec);


        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0, so always add + 1

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var date = dd + '/' + mm + '/' + yyyy;



        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();


        var stuTime = today.getHours() * 60 + today.getMinutes();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var date = dd + '/' + mm + '/' + yyyy;
        date = evDateUtil.convertddMMyyyyToyyyyMMdd(date);

        try {
            console.log(classStTime + " " + stuTime);
            if (false) {

            } else {
                dbUtil.getConnection(function(db) {
                    var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "AttendanceRecords";


                    var query = {
                        "date": date,
                        "email": email,
                        "classStTime": classStTime,
                        "classEndTime": classEndTime,
                        "rollNo": rollNo
                    }

                    db.collection(tableName).remove(query, function(err, result) {
                        res.json({
                            "success": result
                        });
                    });
                });

            }
        } catch (e) {
            console.log(e)
        }
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}

exports.getMarkedStudents = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.stTime && req.body.endTime && req.body.branchId && req.body.subjectId && req.body.year) {

        var tenant = req.body.tenant;

        var classStTime = req.body.stTime;
        var classEndTime = req.body.endTime;

        var branchId = req.body.branchId;

        var subjectId = req.body.subjectId;
        var year = req.body.year;
        var date;
        if (!req.body.date) {
            date = moment().utcOffset("+05:30").format();
            var yyyy = date.substring(0, date.indexOf("-"));
            var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
            var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
            var hh = date.split('T')[1].split(':')[0];
            var minutes = date.split('T')[1].split(':')[1];
            var sec = date.split('T')[1].split(':')[2].substr(0, 2);
            //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
            var today = new Date(yyyy, mm - 1, dd, hh, minutes, sec);


            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0, so always add + 1

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            date = dd + '/' + mm + '/' + yyyy;

            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();


            var today = new Date();
            var stuTime = today.getHours() * 60 + today.getMinutes();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date = dd + '/' + mm + '/' + yyyy;
        } else {
            date = req.body.date;
        }
        date = evDateUtil.convertddMMyyyyToyyyyMMdd(date);
        console.log(date + " " + classStTime + " " + classEndTime);

        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "AttendanceRecords";
                console.log(tableName);
                db.collection(tableName).find({
                    "date": date,
                    "classStTime": classStTime,
                    "classEndTime": classEndTime
                }).toArray(function(err, result) {
                    console.log(result);
                    if (result.length > 0) {
                        res.json({
                            "studentsList": result
                        })
                    } else {
                        res.json({
                            "error": "no student found."
                        })
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}

exports.fetchAttendance = function(req, res) {
    var stReq = req.body;
    if (!(stReq && stReq.tenant && stReq.rollno && stReq.branchId && stReq.subjectId && stReq.year && stReq.startDate && stReq.endDate)) {
        res.json({
            "status": "error",
            "message": "Bad Request"
        });
    }
    var tableName = "T_" + stReq.tenant + "_" + stReq.year + "_" + stReq.branchId + "_" + stReq.subjectId + "_" + "AttendanceRecords";
    var status = stReq.status;
    if (!status) {
        status = "absent";
    }
    var response = {};
    dbUtil.getConnection(function(db) {
        var attendanceCollection = db.collection(tableName);
        attendanceCollection.find({
            date: {
                $gte: stReq.startDate,
                $lte: stReq.endDate
            },
            rollNo: stReq.rollno,
            status: status
        }).toArray(function(err, result) {
            if (err) {
                response.status == "error";
                response.message = "Something went wrong";
            } else {
                response.status = "success";
                response.data = result;
            }
            res.json(response);
        });
    });
};

exports.downloadMonthlyReports = function(req, res) {
    attendanceSvc.studentAttendanceMonthlyStats(req.body).then(function(data) {
        var workbook = excelbuilder.createWorkbook('./', req.body.tenant + "_" + req.body.year + "_" + req.body.branchId + "_" + req.body.subjectId + '_StudentMonthly.xlsx');

    }, function(err) {
        res.json(err);
    });
}

exports.attendanceStatistics = function(req, res) {

    attendanceSvc.studentAttendanceCummStats(req.body).then(function(data) {
        var response = {};
        response.status = "success";
        response.data = data;
        res.json(response);
    }, function(err) {
        res.json(err);
    });


};

exports.mgpsds = function(req, res) {
    migrationSvc.migratePasswords().then(function(data) {
        res.send(data);
    }, function(err) {
        res.send(err);
    });
}

exports.studentMonthlyStats = function(req, res) {

    attendanceSvc.studentAttendanceMonthlyStats(req.body).then(function(data) {
        var response = {};
        response.status = "success";
        response.data = data;
        res.json(response);
    }, function(err) {
        res.json(err);
    });
};

exports.UserAttendanceSubmit = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.subject && req.body.branchName && req.body.faculty && req.body.qrCode && req.body.date && req.body.stTime && req.body.endTime && req.body.rollno && req.body.year && req.body.branchId && req.body.subjectId) {

        var date = req.body.date;
        var classStTime = req.body.stTime;
        var classEndTime = req.body.endTime;
        var rollno = req.body.rollno;
        var tenant = req.body.tenant;
        var year = req.body.year;
        var branchId = req.body.branchId;
        var subjectId = req.body.subjectId;
        var qrCode = req.body.qrCode;
        var faculty = req.body.faculty;
        var subject = req.body.subject;
        var branchName = req.body.branchName;

        console.log(rollno);

        try {


            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId;
                db.collection(tableName).find({}).toArray(function(err, result) {
                    var studentList = [];
                    for (var i = 0; i < result.length; i++) {
                        var stuInfo = {
                            "fname": result[i].name,
                            "rollno": result[i].rollno,
                            "email": result[i].email
                        }
                        studentList.push(stuInfo)
                    }
                    console.log(result);
                    var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "AttendanceRecords";


                    var date = moment().utcOffset("+05:30").format();
                    var yyyy = date.substring(0, date.indexOf("-"));
                    var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
                    var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
                    var hh = date.split('T')[1].split(':')[0];
                    var minutes = date.split('T')[1].split(':')[1];
                    var sec = date.split('T')[1].split(':')[2].substr(0, 2);
                    //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
                    var today = new Date(yyyy, mm - 1, dd, hh, minutes, sec);


                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0, so always add + 1

                    var yyyy = today.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    var date = dd + '/' + mm + '/' + yyyy;

                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();


                    var stuTime = today.getHours() * 60 + today.getMinutes();

                    //var stuTime = today.getHours() * 60 + today.getMinutes();

                    date = evDateUtil.convertddMMyyyyToyyyyMMdd(date);
                    async.eachOfSeries(studentList, function(item, key, callback) {
                        var data = {
                            "qrCode": qrCode,
                            "faculty": faculty,
                            "classStTime": classStTime,
                            "classEndTime": classEndTime,
                            "stuName": item.fname,
                            "branchName": branchName,
                            "subject": subject,
                            "year": year,
                            "rollNo": item.rollno,
                            "email": item.email,
                            "stuTime": stuTime,
                            "date": date,
                        }
                        if (rollno.indexOf(item.rollno) != -1) {

                            db.collection(tableName).find({
                                "date": date,
                                "classStTime": classStTime,
                                "classEndTime": classEndTime,
                                "rollNo": item.rollno
                            }).toArray(function(err, result) {
                                console.log(result);
                                if (result.length > 0) {
                                    db.collection(tableName).updateOne({
                                        "date": date,
                                        "classStTime": classStTime,
                                        "classEndTime": classEndTime,
                                        "rollNo": item.rollno
                                    }, {
                                        $set: {
                                            'status': 'present'
                                        }
                                    }, function(err, result) {
                                        //db.close();
                                    });
                                } else {
                                    data.status = "present";
                                    db.collection(tableName).insertOne(data, function(err, result3) {
                                        //db.close();
                                    });
                                }
                            });
                        } else {
                            db.collection(tableName).find({
                                "date": date,
                                "classStTime": classStTime,
                                "classEndTime": classEndTime,
                                "rollNo": item.rollno
                            }).toArray(function(err, result) {
                                console.log(result);
                                if (result.length > 0) {

                                } else {
                                    data.status = "absent";
                                    db.collection(tableName).insertOne(data, function(err, result3) {
                                        //db.close();
                                    });
                                }
                            });
                        }
                        callback();
                    }, function(err) {
                        console.log("err");
                        console.log(err);
                        if (!err) {
                            res.json({
                                "success": "Attendance Submitted"
                            })
                        } else {
                            res.json({
                                "error": "Attendance notSubmitted"
                            })
                        }
                    });
                });


            });

        } catch (e) {
            console.log(e);
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}

/*exports.AddOnUserAttendanceSubmit = function(req, res, next) {
 if (req.body && req.body.tenant && req.body.subject && req.body.branchName && req.body.faculty && req.body.qrCode && req.body.sheetId && req.body.date && req.body.stTime && req.body.endTime && req.body.email && req.body.year && req.body.branchId && req.body.subjectId) {

 var date = req.body.date;
 var classStTime = req.body.stTime;
 var classEndTime = req.body.endTime;
 var email = req.body.email;
 var tenant = req.body.tenant;
 var year = req.body.year;
 var branchId = req.body.branchId;
 var subjectId = req.body.subjectId;
 var sheetId = req.body.sheetId;
 var qrCode = req.body.qrCode;
 var faculty = req.body.faculty;
 var subject = req.body.subject;
 var branchName = req.body.branchName;


 var my_sheet = new GoogleSpreadsheet(req.body.sheetId);

 try {
 my_sheet.getRows(1, function(err, row_data) {
 var studentList = [];

 for (var i = 0; i < row_data.length; i++) {
 var stuInfo = {
 "fname": row_data[i].firstname,
 "lname": row_data[i].lastname,
 "rollno": row_data[i].rollno,
 "email": row_data[i].email
 }
 studentList.push(stuInfo)

 }
 console.log(studentList);
 var i = 0;

 dbUtil.getConnection(function(db) {
 var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId;
 var updateAddAttendance = function(i) {
 if (i < studentList.length) {
 //console.log(studentList[i]);
 var today = new Date();
 var stuTime = today.getHours() * 60 + today.getMinutes();
 var data = {
 "qrCode": qrCode,
 "faculty": faculty,
 "classStTime": classStTime,
 "classEndTime": classEndTime,
 "stuName": studentList[i].fname + " " + studentList[i].lname,
 "branchName": branchName,
 "subject": subject,
 "year": "1",
 "rollNo": studentList[i].rollno,
 "email": studentList[i].email,
 "stuTime": stuTime,
 "date": date,
 }

 console.log(studentList[i].email+" "+email.indexOf(studentList[i].email));
 if (email.indexOf(studentList[i].email) != -1) {
 db.collection(tableName).find({
 "date": date,
 "classStTime": classStTime,
 "classEndTime": classEndTime,
 "email": email
 }).toArray(function(err, result) {
 console.log(result);
 if (result.length > 0) {
 db.collection(tableName).updateOne({
 "date": date,
 "classStTime": classStTime,
 "classEndTime": classEndTime,
 "email": email
 }, {
 $set: {
 'status': 'present'
 }
 }, function(err, result) {
 if (!err) {

 i = i + 1;
 updateAddAttendance(i);
 }
 });
 } else {
 data.status = "present";
 db.collection(tableName).insertOne(data, function(err, result3) {
 if (!err) {
 i = i + 1;
 updateAddAttendance(i);
 }
 });
 }
 });


 } else {
 data.status = "absent";
 db.collection(tableName).insertOne(data, function(err, result3) {
 if (!err) {
 i = i + 1;
 updateAddAttendance(i);
 }
 });
 }

 } else {
 res.json({
 "success": "Attendance marked"
 })
 }
 }

 updateAddAttendance(i);
 });
 });
 } catch (e) {
 res.json({
 "error": "Some error occurred. Please try again."
 });
 }
 } else {
 res.status(401).json({
 "error": "Parameters missing"
 });
 }
 }*/


exports.AddOnCancelClass = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.date && req.body.stTime && req.body.endTime && req.body.email && req.body.year && req.body.branchId && req.body.subjectId) {

        var date = req.body.date;
        var classStTime = req.body.stTime;
        var classEndTime = req.body.endTime;
        var email = req.body.email;
        var tenant = req.body.tenant;
        var year = req.body.year;
        var branchId = req.body.branchId;
        var subjectId = req.body.subjectId;
        date = evDateUtil.convertddMMyyyyToyyyyMMdd(date);
        try {
            var query = {
                'date': date,
                'classStTime': classStTime,
                'classEndTime': classEndTime,
                'email': {
                    $in: email
                }
            };
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "AttendanceRecords";
                db.collection(tableName).remove(query, {
                    writeConcern: 1
                }, function(err, result) {
                    console.log(result);
                    if (result.result.ok === 1) {
                        res.json({
                            "success": "success"
                        });
                    } else {
                        res.json({
                            "error": "Cancellation not working. Please try after sometime."
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.UserCancelClass = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.date && req.body.StTime && req.body.EndTime && req.body.rollNo && req.body.year && req.body.branchId && req.body.subjectId) {

        var date = req.body.date;
        var classStTime = req.body.StTime;
        var classEndTime = req.body.EndTime;
        var rollNo = req.body.rollNo;
        var tenant = req.body.tenant;
        var year = req.body.year;
        var branchId = req.body.branchId;
        var subjectId = req.body.subjectId;
        date = evDateUtil.convertddMMyyyyToyyyyMMdd(date);
        try {
            var query = {
                'date': date,
                'classStTime': classStTime,
                'classEndTime': classEndTime,
                'rollNo': {
                    $in: rollNo
                }
            };
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "UserCancelClass";
                db.collection(tableName).remove(query, {
                    writeConcern: 1
                }, function(err, result) {
                    if (result.acknowledged === true) {
                        res.json({
                            "success": "success"
                        });
                    } else {
                        res.json({
                            "error": "Cancellation not working. Please try again."
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.cancelClass = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.date && req.body.StTime && req.body.EndTime && req.body.rollNo && req.body.year && req.body.branchId && req.body.subjectId) {

        var date = req.body.date;
        var classStTime = req.body.StTime;
        var classEndTime = req.body.EndTime;
        var rollNo = req.body.rollNo;
        var tenant = req.body.tenant;
        var year = req.body.year;
        var branchId = req.body.branchId;
        var subjectId = req.body.subjectId;
        date = evDateUtil.convertddMMyyyyToyyyyMMdd(date);
        try {
            var query = {
                'date': date,
                'classStTime': classStTime,
                'classEndTime': classEndTime,
                'rollNo': {
                    $in: rollNo
                }
            };
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_CancelledClasses";
                db.collection(tableName).remove(query, {
                    writeConcern: 1
                }, function(err, result) {
                    if (result.acknowledged === true) {
                        res.json({
                            "success": "success"
                        });
                    } else {
                        res.json({
                            "error": "Cancellation not working. Please try again."
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.createExcel = function(req, res, next) {
    // Create a new workbook file in current working-path
    if (req.body && req.body.tenant && req.body.year && req.body.branchId && req.body.subjectId) {
        var year = req.body.year;
        var branchId = req.body.branchId;
        var subjectId = req.body.subjectId;
        var tenant = req.body.tenant;

        var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId;
        var workbook = excelbuilder.createWorkbook('./', tableName + '_StudentAttendance.xlsx')

        // Create a new worksheet with 10 columns and 12 rows

        // Fill some data

        dbUtil.getConnection(function(db) {
            var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "AttendanceRecords";
            db.collection(tableName).find({}).toArray(function(err, result) {
                console.log(result);
                if (result.length > 0) {
                    var sheet1 = workbook.createSheet('StudentsAttendance', 13, result.length + 2);
                    sheet1.set(1, 1, "Date");
                    sheet1.set(2, 1, "Student Name");
                    sheet1.set(3, 1, "Roll No");
                    sheet1.set(4, 1, "Email");
                    sheet1.set(5, 1, "Class");
                    sheet1.set(6, 1, "Subject");
                    sheet1.set(7, 1, "Semester");
                    sheet1.set(8, 1, "Faculty");
                    sheet1.set(9, 1, "Student Time");
                    sheet1.set(10, 1, "Qr Used");
                    sheet1.set(11, 1, "Status");


                    sheet1.font(1, 1, {
                        "name": "Cambria (Headings)",
                        "sz": "12",
                        "bold": "true"
                    });
                    sheet1.font(2, 1, {
                        "name": "Cambria (Headings)",
                        "sz": "12",
                        "bold": "true"
                    });
                    sheet1.font(3, 1, {
                        "name": "Cambria (Headings)",
                        "sz": "12",
                        "bold": "true"
                    });
                    sheet1.font(4, 1, {
                        "name": "Cambria (Headings)",
                        "sz": "12",
                        "bold": "true"
                    });
                    sheet1.font(5, 1, {
                        "name": "Cambria (Headings)",
                        "sz": "12",
                        "bold": "true"
                    });
                    sheet1.font(6, 1, {
                        "name": "Cambria (Headings)",
                        "sz": "12",
                        "bold": "true"
                    });
                    sheet1.font(7, 1, {
                        "name": "Cambria (Headings)",
                        "sz": "12",
                        "bold": "true"
                    });
                    sheet1.font(8, 1, {
                        "name": "Cambria (Headings)",
                        "sz": "12",
                        "bold": "true"
                    });
                    sheet1.font(9, 1, {
                        "name": "Cambria (Headings)",
                        "sz": "12",
                        "bold": "true"
                    });
                    sheet1.font(10, 1, {
                        "name": "Cambria (Headings)",
                        "sz": "12",
                        "bold": "true"
                    });
                    sheet1.font(11, 1, {
                        "name": "Cambria (Headings)",
                        "sz": "12",
                        "bold": "true"
                    });


                    for (var i = 0; i < result.length; i++) {
                        try {
                            console.log(result[i].date);
                            sheet1.set(1, i + 2, result[i].date);
                            sheet1.set(2, i + 2, result[i].stuName);
                            sheet1.set(3, i + 2, result[i].rollNo);
                            sheet1.set(4, i + 2, result[i].email);
                            sheet1.set(5, i + 2, result[i].branchName);
                            sheet1.set(6, i + 2, result[i].subject);
                            sheet1.set(7, i + 2, result[i].year);
                            sheet1.set(8, i + 2, result[i].faculty);
                            sheet1.set(9, i + 2, result[i].stuTime);
                            sheet1.set(10, i + 2, result[i].qrCode);
                            sheet1.set(11, i + 2, result[i].status);

                        } catch (e) {}
                    }
                    // Save it

                    workbook.save(function(err) {
                        if (err) {
                            res.send(err);
                        } else {
                            console.log("sheet created");
                            res.send({
                                'success': 'sheet created'
                            });
                        }

                    });

                } else {
                    res.send({
                        "Error": "No attendance data found."
                    });
                }
            });
        });


    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }

}


/*
 exports.AddOnUserAttendanceSubmit = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.subject && req.body.branchName && req.body.faculty && req.body.qrCode && req.body.sheetId && req.body.date && req.body.stTime && req.body.endTime && req.body.email && req.body.year && req.body.branchId && req.body.subjectId) {

        var date = req.body.date;
        var classStTime = req.body.stTime;
        var classEndTime = req.body.endTime;
        var email = req.body.email;
        var tenant = req.body.tenant;
        var year = req.body.year;
        var branchId = req.body.branchId;
        var subjectId = req.body.subjectId;
        var sheetId = req.body.sheetId;
        var qrCode = req.body.qrCode;
        var faculty = req.body.faculty;
        var subject = req.body.subject;
        var branchName = req.body.branchName;

        console.log(email);
       // var my_sheet = new GoogleSpreadsheet(req.body.sheetId);

        try {
            my_sheet.getRows(1, function (err, row_data) {
                var studentList = [];
                for (var i = 0; i < row_data.length; i++) {
                    var stuInfo = {
                        "fname": row_data[i].firstname,
                        "lname": row_data[i].lastname,
                        "rollno": row_data[i].rollno,
                        "email": row_data[i].email
                    }
                    studentList.push(stuInfo)
                }
                //console.log(studentList);
                var callback = function () {

                }

                //console.log(item);
                dbUtil.getConnection(function (db) {
                    var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "AttendanceRecords";


                    var date = moment().utcOffset("+05:30").format();
                    var yyyy = date.substring(0, date.indexOf("-"));
                    var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
                    var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
                    var hh = date.split('T')[1].split(':')[0];
                    var minutes = date.split('T')[1].split(':')[1];
                    var sec = date.split('T')[1].split(':')[2].substr(0, 2);
                    //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
                    var today = new Date(yyyy, mm - 1, dd, hh, minutes, sec);


                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0, so always add + 1

                    var yyyy = today.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    var date = dd + '/' + mm + '/' + yyyy;

                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();


                    var stuTime = today.getHours() * 60 + today.getMinutes();
                    //var stuTime = today.getHours() * 60 + today.getMinutes();


                    async.eachOfSeries(studentList, function (item, key, callback) {
                        var data = {
                            "qrCode": qrCode,
                            "faculty": faculty,
                            "classStTime": classStTime,
                            "classEndTime": classEndTime,
                            "stuName": item.fname + " " + item.lname,
                            "branchName": branchName,
                            "subject": subject,
                            "year": year,
                            "rollNo": item.rollno,
                            "email": item.email,
                            "stuTime": stuTime,
                            "date": date,
                        }
                        if (email.indexOf(item.email) != -1) {

                            db.collection(tableName).find({
                                "date": date,
                                "classStTime": classStTime,
                                "classEndTime": classEndTime,
                                "email": item.email
                            }).toArray(function (err, result) {
                                console.log(result);
                                if (result.length > 0) {
                                    db.collection(tableName).updateOne({
                                        "date": date,
                                        "classStTime": classStTime,
                                        "classEndTime": classEndTime,
                                        "email": item.email
                                    }, {
                                        $set: {
                                            'status': 'present'
                                        }
                                    }, function (err, result) {
                                        //db.close();
                                    });
                                } else {
                                    data.status = "present";
                                    db.collection(tableName).insertOne(data, function (err, result3) {
                                        //db.close();
                                    });
                                }
                            });
                        } else {
                            db.collection(tableName).find({
                                "date": date,
                                "classStTime": classStTime,
                                "classEndTime": classEndTime,
                                "email": item.email
                            }).toArray(function (err, result) {
                                console.log(result);
                                if (result.length > 0) {

                                } else {
                                    data.status = "absent";
                                    db.collection(tableName).insertOne(data, function (err, result3) {
                                        //db.close();
                                    });
                                }
                            });
                        }
                        callback();
                    }, function (err) {
                        console.log("err");
                        console.log(err);
                        if (!err) {
                            res.json({
                                "success": "Attendance Submitted"
                            })
                        } else {
                            res.json({
                                "error": "Attendance notSubmitted"
                            })
                        }
                    });
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}
*/


/*
 exports.AddOnUserAttendanceAllPresent = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.subject && req.body.branchName && req.body.faculty && req.body.qrCode && req.body.sheetId && req.body.date && req.body.stTime && req.body.endTime && req.body.email && req.body.year && req.body.branchId && req.body.subjectId) {

        var date = req.body.date;
        var classStTime = req.body.stTime;
        var classEndTime = req.body.endTime;
        var email = req.body.email;
        var tenant = req.body.tenant;
        var year = req.body.year;
        var branchId = req.body.branchId;
        var subjectId = req.body.subjectId;
        var sheetId = req.body.sheetId;
        var qrCode = req.body.qrCode;
        var faculty = req.body.faculty;
        var subject = req.body.subject;
        var branchName = req.body.branchName;

        console.log(email);
        var my_sheet = new GoogleSpreadsheet(req.body.sheetId);

        try {
            my_sheet.getRows(1, function (err, row_data) {
                var studentList = [];
                for (var i = 0; i < row_data.length; i++) {
                    var stuInfo = {
                        "fname": row_data[i].firstname,
                        "lname": row_data[i].lastname,
                        "rollno": row_data[i].rollno,
                        "email": row_data[i].email
                    }
                    studentList.push(stuInfo)
                }
                //console.log(studentList);
                var callback = function () {

                }

                //console.log(item);
                dbUtil.getConnection(function (db) {
                    var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "AttendanceRecords";


                    var date = moment().utcOffset("+05:30").format();
                    var yyyy = date.substring(0, date.indexOf("-"));
                    var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
                    var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
                    var hh = date.split('T')[1].split(':')[0];
                    var minutes = date.split('T')[1].split(':')[1];
                    var sec = date.split('T')[1].split(':')[2].substr(0, 2);
                    //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
                    var today = new Date(yyyy, mm - 1, dd, hh, minutes, sec);


                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0, so always add + 1

                    var yyyy = today.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    var date = dd + '/' + mm + '/' + yyyy;

                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();


                    var stuTime = today.getHours() * 60 + today.getMinutes();
                    //var stuTime = today.getHours() * 60 + today.getMinutes();


                    async.eachOfSeries(studentList, function (item, key, callback) {
                        var data = {
                            "qrCode": qrCode,
                            "faculty": faculty,
                            "classStTime": classStTime,
                            "classEndTime": classEndTime,
                            "stuName": item.fname + " " + item.lname,
                            "branchName": branchName,
                            "subject": subject,
                            "year": year,
                            "rollNo": item.rollno,
                            "email": item.email,
                            "stuTime": stuTime,
                            "date": date,
                        }
                        if (email.indexOf(item.email) != -1) {

                            db.collection(tableName).find({
                                "date": date,
                                "classStTime": classStTime,
                                "classEndTime": classEndTime,
                                "email": item.email
                            }).toArray(function (err, result) {
                                console.log(result);
                                if (result.length > 0) {

                                } else {
                                    data.status = "absent";
                                    db.collection(tableName).insertOne(data, function (err, result3) {
                                        //db.close();
                                    });
                                }
                            });
                        } else {

                        }
                        callback();
                    }, function (err) {
                        console.log("err");
                        console.log(err);
                        if (!err) {
                            res.json({
                                "success": "Attendance Submitted"
                            })
                        } else {
                            res.json({
                                "error": "Attendance notSubmitted"
                            })
                        }
                    });
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}
*/


exports.UserAttendanceAllPresent = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.subject && req.body.branchName && req.body.faculty && req.body.qrCode && req.body.date && req.body.stTime && req.body.endTime && req.body.rollno && req.body.year && req.body.branchId && req.body.subjectId) {

        var date = req.body.date;
        var classStTime = req.body.stTime;
        var classEndTime = req.body.endTime;
        var rollno = req.body.rollno;
        var tenant = req.body.tenant;
        var year = req.body.year;
        var branchId = req.body.branchId;
        var subjectId = req.body.subjectId;
        var sheetId = req.body.sheetId;
        var qrCode = req.body.qrCode;
        var faculty = req.body.faculty;
        var subject = req.body.subject;
        var branchName = req.body.branchName;

        console.log(rollno);

        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId;
                db.collection(tableName).find({}).toArray(function(err, result) {
                    if (result.length > 0) {

                        var studentsList = result


                        dbUtil.getConnection(function(db) {
                            var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "AttendanceRecords";


                            var date = moment().utcOffset("+05:30").format();
                            var yyyy = date.substring(0, date.indexOf("-"));
                            var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
                            var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
                            var hh = date.split('T')[1].split(':')[0];
                            var minutes = date.split('T')[1].split(':')[1];
                            var sec = date.split('T')[1].split(':')[2].substr(0, 2);
                            //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
                            var today = new Date(yyyy, mm - 1, dd, hh, minutes, sec);


                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0, so always add + 1

                            var yyyy = today.getFullYear();
                            if (dd < 10) {
                                dd = '0' + dd
                            }
                            if (mm < 10) {
                                mm = '0' + mm
                            }
                            var date = dd + '/' + mm + '/' + yyyy;

                            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();


                            var stuTime = today.getHours() * 60 + today.getMinutes();
                            //var stuTime = today.getHours() * 60 + today.getMinutes();

                            date = evDateUtil.convertddMMyyyyToyyyyMMdd(date);
                            async.eachOfSeries(studentsList, function(item, key, callback) {
                                var data = {
                                        "qrCode": qrCode,
                                        "faculty": faculty,
                                        "classStTime": classStTime,
                                        "classEndTime": classEndTime,
                                        "stuName": item.name,
                                        "branchName": branchName,
                                        "subject": subject,
                                        "year": year,
                                        "rollNo": item.rollno,
                                        "email": item.email,
                                        "stuTime": stuTime,
                                        "date": date,
                                    }
                                    //if (rollno.indexOf(item.rollno) != -1) {

                                db.collection(tableName).find({
                                    "date": date,
                                    "classStTime": classStTime,
                                    "classEndTime": classEndTime,
                                    "rollNo": item.rollno
                                }).toArray(function(err, result) {
                                    console.log(result);
                                    if (result.length > 0) {

                                    } else {
                                        data.status = "absent";
                                        db.collection(tableName).insertOne(data, function(err, result3) {
                                            //db.close();
                                        });
                                    }
                                });
                                //} else {}
                                callback();
                            }, function(err) {
                                console.log(err);
                                if (!err) {
                                    res.json({
                                        "success": "Attendance Submitted"
                                    })
                                } else {
                                    res.json({
                                        "error": "Attendance notSubmitted"
                                    })
                                }
                            });
                        });
                    } else {
                        res.json({
                            "error": "No Students in the list"
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }


    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.submitClassesRecord = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.date && req.body.stTime && req.body.endTime && req.body.year && req.body.branchId && req.body.subjectId) {
        var data = {
            "date": req.body.date,
            "stTime": req.body.stTime,
            "endTime": req.body.endTime,
            "year": req.body.year,
            "branchId": req.body.branchId,
            "subjectId": req.body.subjectId
        }

        dbUtil.getConnection(function(db) {
            var tableName = "T_" + req.body.tenant + "_SubmitClassRecords";
            db.collection(tableName).find(data).toArray(function(err, result) {
                if (result.length > 0) {
                    res.json({
                        "error": "Class is submitted already"
                    });
                } else {
                    db.collection(tableName).insertOne(data, function(err, result3) {
                        if (!err) {
                            res.json({
                                "success": "Class submitted"
                            });
                        }
                    });
                }
            });
        });
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}

exports.checkSubmitClassesRecord = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.date && req.body.stTime && req.body.endTime && req.body.year && req.body.branchId && req.body.subjectId) {
        var data = {
            "date": req.body.date,
            "stTime": req.body.stTime,
            "endTime": req.body.endTime,
            "year": req.body.year,
            "branchId": req.body.branchId,
            "subjectId": req.body.subjectId
        }

        dbUtil.getConnection(function(db) {
            var tableName = "T_" + req.body.tenant + "_SubmitClassRecords";
            db.collection(tableName).find(data).toArray(function(err, result) {
                if (result.length > 0) {
                    res.json({
                        "error": "Class is submitted already"
                    });
                } else {
                    res.json({
                        "success": "not submitted"
                    });
                }
            });
        });
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.cancelClassesRecord = function(req, res, next) {
    if (req.body && req.body.tenant && req.body.reason && req.body.date && req.body.branchName && req.body.subject && req.body.stTime && req.body.endTime && req.body.year && req.body.branchId && req.body.subjectId && req.body.facultyEmail) {
        var data = {
            "date": req.body.date,
            "stTime": req.body.stTime,
            "endTime": req.body.endTime,
            "year": req.body.year,
            "branchId": req.body.branchId,
            "subjectId": req.body.subjectId,
            "branchName": req.body.branchName,
            "subject": req.body.subject,
            "tenant": req.body.tenant,
            "facultyEmail": req.body.facultyEmail,
            "facultyName": req.body.facultyName

        }

        dbUtil.getConnection(function(db) {
            var tableName = "T_" + req.body.tenant + "_cancelClassRecords";
            db.collection(tableName).find(data).toArray(function(err, result) {
                if (result.length > 0) {
                    res.json({
                        "error": "Class is already cancelled"
                    });
                } else {
                    data.reason = req.body.reason;
                    db.collection(tableName).insertOne(data, function(err, result3) {
                        if (!err) {
                            res.json({
                                "success": "Class cancelled"
                            });
                        }
                    });
                }
            });
        });
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.getcancelClassesRecord = function(req, res, next) {
    if (req.body && req.body.tenant) {
        var tenant = req.body.tenant
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_cancelClassRecords";
                db.collection(tableName).find({
                    "tenant": tenant
                }).toArray(function(err, result) {
                    if (result.length > 0) {
                        res.json({
                            'cancelclasses': result
                        });
                    } else {
                        res.json({
                            "error": "No cancel classes"
                        });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}

/*
 exports.testAsync = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.subject && req.body.branchName && req.body.faculty && req.body.qrCode && req.body.sheetId && req.body.date && req.body.stTime && req.body.endTime && req.body.email && req.body.year && req.body.branchId && req.body.subjectId) {

        var date = req.body.date;
        var classStTime = req.body.stTime;
        var classEndTime = req.body.endTime;
        var email = req.body.email;
        var tenant = req.body.tenant;
        var year = req.body.year;
        var branchId = req.body.branchId;
        var subjectId = req.body.subjectId;
        var sheetId = req.body.sheetId;
        var qrCode = req.body.qrCode;
        var faculty = req.body.faculty;
        var subject = req.body.subject;
        var branchName = req.body.branchName;

        console.log(email);
        var my_sheet = new GoogleSpreadsheet(req.body.sheetId);

        try {
            my_sheet.getRows(1, function (err, row_data) {
                var studentList = [];
                for (var i = 0; i < row_data.length; i++) {
                    var stuInfo = {
                        "fname": row_data[i].firstname,
                        "lname": row_data[i].lastname,
                        "rollno": row_data[i].rollno,
                        "email": row_data[i].email
                    }
                    studentList.push(stuInfo)
                }
                //console.log(studentList);
                var callback = function () {

                }

                //console.log(item);
                dbUtil.getConnection(function (db) {
                    var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "testAsync";


                    var date = moment().utcOffset("+05:30").format();
                    var yyyy = date.substring(0, date.indexOf("-"));
                    var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
                    var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
                    var hh = date.split('T')[1].split(':')[0];
                    var minutes = date.split('T')[1].split(':')[1];
                    var sec = date.split('T')[1].split(':')[2].substr(0, 2);
                    //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
                    var today = new Date(yyyy, mm - 1, dd, hh, minutes, sec);


                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0, so always add + 1

                    var yyyy = today.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    var date = dd + '/' + mm + '/' + yyyy;

                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();


                    var stuTime = today.getHours() * 60 + today.getMinutes();
                    //var stuTime = today.getHours() * 60 + today.getMinutes();


                    async.eachOfSeries(studentList, function (item, key, callback) {
                        var data = {
                            "qrCode": qrCode,
                            "faculty": faculty,
                            "classStTime": classStTime,
                            "classEndTime": classEndTime,
                            "stuName": item.fname + " " + item.lname,
                            "branchName": branchName,
                            "subject": subject,
                            "year": year,
                            "rollNo": item.rollno,
                            "email": item.email,
                            "stuTime": stuTime,
                            "date": date,
                        }
                        if (email.indexOf(item.email) != -1) {

                            db.collection(tableName).find({
                                "date": date,
                                "classStTime": classStTime,
                                "classEndTime": classEndTime,
                                "email": item.email
                            }).toArray(function (err, result) {
                                console.log(result);
                                if (result.length > 0) {
                                    db.collection(tableName).updateOne({
                                        "date": date,
                                        "classStTime": classStTime,
                                        "classEndTime": classEndTime,
                                        "email": item.email
                                    }, {
                                        $set: {
                                            'status': 'present'
                                        }
                                    }, function (err, result) {
                                        //db.close();
                                    });
                                } else {
                                    data.status = "present";
                                    db.collection(tableName).insertOne(data, function (err, result3) {
                                        //db.close();
                                    });
                                }
                            });
                        } else {
                            db.collection(tableName).find({
                                "date": date,
                                "classStTime": classStTime,
                                "classEndTime": classEndTime,
                                "email": item.email
                            }).toArray(function (err, result) {
                                console.log(result);
                                if (result.length > 0) {

                                } else {
                                    data.status = "absent";
                                    db.collection(tableName).insertOne(data, function (err, result3) {
                                        //db.close();
                                    });
                                }
                            });
                        }
                        callback();
                    }, function (err) {
                        console.log("err");
                        console.log(err);
                        if (!err) {
                            res.json({
                                "success": "Attendance Submitted"
                            })
                        } else {
                            res.json({
                                "error": "Attendance notSubmitted"
                            })
                        }
                    });
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}
*/


exports.getAttendanceRoster = function(req, res, next) {
    // Create a new workbook file in current working-path
    if (req.body && req.body.tenant && req.body.year && req.body.branchId && req.body.subjectId) {
        var year = req.body.year;
        var branchId = req.body.branchId;
        var subjectId = req.body.subjectId;
        var tenant = req.body.tenant;

        var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "AttendanceRecords";

        // Create a new worksheet with 10 columns and 12 rows

        // Fill some data

        dbUtil.getConnection(function(db) {
            //var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId;
            db.collection(tableName).find({
                date: req.body.date,
                classStTime: req.body.stTime,
                classEndTime: req.body.endTime
            }).toArray(function(err, result) {
                console.log(result);
                if (result.length > 0) {

                    res.send({
                        "roster": result
                    });
                } else {
                    res.send({
                        "Error": "No attendance data found."
                    });
                }
            });
        });


    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }

}

exports.downloadStatsReports = function(req, res) {
    var reqObj = req.body;
    attendanceSvc.studentAttendanceMonthlyStats(reqObj).then(function(result) {

            var filename = __dirname + '/../../downloads/' + reqObj.tenant + reqObj.subjectId + reqObj.branchId + ".xlsx";
            console.log("result length", result.length);
            if (result.length > 0) {
                //console.log("before wrire stream");

                //res.attachment('user-file.xls');
                var header = '#' + '\t' + 'Student Name' + '\t' + 'Roll No';
                var months = result[0].monthsStat;
                for (var i = 0; i < months.length; i++) {
                    header = header + "\t" + months[i].month;
                }
                header = header + "\t" + "present/total" + "\n";
                var row = '';

                console.log("before row ..");
                for (var i = 0; i < result.length; i++) {
                    try {
                        row = row + i + '\t' + result[i]._id.stuName + '\t' + result[i]._id.rollNo;
                        var stuMonthsStats = result[i].monthsStat;
                        for (var j = 0; j < stuMonthsStats.length; j++) {
                            row = row + "\t" + stuMonthsStats[j].present + "/" + stuMonthsStats[j].total;
                        }
                        row = row + "\t" + result[i].totalPresent + "/" + result[j].total + "\n";

                    } catch (e) {
                        console.log("error", e);
                    }
                }

                var data = header + row;
                /*res.setHeader('Content-disposition', 'attachment; filename=' + filename);
                res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                writeStream.pipe(res);*/
                //console.log("dddd");
                /*  writeStream.on('end', function() {
                      console.log("Closing stream");
                      res.setHeader('Content-disposition', 'attachment; filename=' + filename);
                      //res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                      writeStream.close();
                      res.end();
                  });*/
                var writeStream;
                try {
                    writeStream = fs.createWriteStream(filename);
                    writeStream.write(data);
                    writeStream.on('finish', () => {
                        console.log('wrote all data to file');
                    });

                } catch (e) {
                    console.log("dddd", e);
                }

                console.log("ddd___");

                //res.end();
                writeStream.end();

                res.download(filename);

                /*res.write(data);
                res.end();*/


                /*
                   res.setHeader('Content-disposition', 'attachment; filename=' + filename);
                    res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    res.write(data);
                writeStream.write(data);
               res.end();*/
                //writeStream.end();
                //res.download(file);
            } else {
                res.send("nothing to write");
            }
        },
        function(err) {
            res.json(err);
        });


};

exports.downloadReport = function(req, res, next) {
    // Create a new workbook file in current working-path
    if (req.body && req.body.tenant && req.body.year && req.body.branchId && req.body.subjectId) {
        var year = req.body.year;
        var branchId = req.body.branchId;
        var subjectId = req.body.subjectId;
        var tenant = req.body.tenant;


        dbUtil.getConnection(function(db) {
            var tableName = "T_" + tenant + "_" + year + "_" + branchId + "_" + subjectId + "_" + "AttendanceRecords";
            db.collection(tableName).find({
                date: req.body.date,
                classStTime: req.body.stTime,
                classEndTime: req.body.endTime
            }).toArray(function(err, result) {
                console.log(result);
                if (result.length > 0) {
                    var file = __dirname + '/../../public/' + tenant + '_attendance.xls';
                    var writeStream = fs.createWriteStream(file);
                    //res.attachment('user-file.xls');
                    var header = 'Date' + '\t' + 'Student Name' + '\t' + 'Roll No' + '\t' + 'Email' + '\t' + 'Class' + '\t' + 'Subject' + '\t' + 'Semester' + '\t' + 'Faculty' + '\t' + 'Student Time' + '\t' + 'Qr Used' + '\t' + 'Status' + '\n';

                    var row = '';

                    for (var i = 0; i < result.length; i++) {
                        try {

                            row = row + result[i].date + '\t' + result[i].stuName + '\t' + result[i].rollNo + '\t' + result[i].email +
                                '\t' + result[i].branchName + '\t' + result[i].subject + '\t' + result[i].year + '\t' + result[i].faculty + '\t' + result[i].stuTime + '\t' + result[i].qrCode + '\t' + result[i].status + '\n';

                        } catch (e) {}
                    }

                    var data = header + row;

                    writeStream.write(data);
                    writeStream.close();
                    res.send({
                        success: true
                    })
                } else {
                    res.send({
                        "Error": "No attendance data found."
                    });
                }
            });
        });


    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }

}

exports.downloadReportTenant = function(req, res) {

    console.log('download ')

    var tenant = req.params.tenant;
    var file = __dirname + '/../../public/' + tenant + '_attendance.xls';

    res.download(file);

}