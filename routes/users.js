var express = require('express');
var router = express.Router();
var bcryptUtils = require('../utils/bcryptutils');
var dbUtil = require("../config/dbUtil");
const saltRounds = 10;

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;


/* GET users listing. */
router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});


// Register
router.get('/register', function(req, res) {
    console.log("register calledd");
    res.render('register');
});

// Login
router.get('/login', function(req, res) {
    res.render('login');
});

// Register User
router.post('/register', function(req, res) {
    var name = req.body.name;
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;
    var role = req.body.role;
    var tenant = req.body.tenant;
    //var password2 = req.body.password2;

    // Validation
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email is not valid').isEmail();
    req.checkBody('username', 'Username is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();
    //req.checkBody('password2', 'Passwords do not match').equals(req.body.password);

    var errors = req.validationErrors();
    console.log("errors : : ", errors);
    if (errors) {
        res.render('register', {
            errors: errors
        });
    } else {
        bcryptUtils.encryptPassword(password).then(function(data) {
            var newUser = {
                name: name,
                email: email,
                username: username,
                role:role,
                tenant:tenant,
                hash: data.hash,
                salt: data.salt
            };
            console.log("News user : ",newUser);
            dbUtil.getConnection(function(db) {
                var collection = db.collection("T_UserRecords");
                collection.insert(newUser, function(err, data) {
                    if (err) {
                        res.render('register', {
                            errors: "Failed to register"
                        });
                    }
                });
            });

        }, function(err) {
            res.render('register', {
                errors: "Failed to register"
            });
        });


        /*      var newUser = new User({
                    name: name,
                    email: email,
                    username: username,
                    password: password
                });

                User.createUser(newUser, function(err, user) {
                    if (err) throw err;
                    console.log(user);
                });
        */
        req.flash('success_msg', 'You are registered and can now login');

        res.redirect('/kryptosattendance/users/login');
    }
});

router.get('/logout', function(req, res) {
    req.logout();

    req.flash('success_msg', 'You are logged out');

    res.redirect('/kryptosattendance/dashboard/login');
});



module.exports = router;